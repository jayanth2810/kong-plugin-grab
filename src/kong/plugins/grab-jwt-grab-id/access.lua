local responses = require "kong.tools.responses"
local http = require "socket.http"
local https = require "ssl.https"
local json = require "JSON"
local ltn12 = require 'ltn12'

local _M = {}

local constants = {
  ['GRAB_ID_KEY'] = 'Grab_ID',
  ['JWT_KEY'] = 'Authorization',
  ['GRAB_ID_RESPONSE_KEY'] = 'UserID',
}

function _M.process_headers()
  local headers = ngx.req.get_headers()
  headers['host'] = nil
  headers['accept-encoding'] = nil
  headers['Content-Type'] = 'application/json; charset=utf-8'
  return headers
end

function _M.execute(conf)
  local res = {}
  headers = _M.process_headers()
  local r, status_code, h, s = https.request{
    url = conf.api_url,
    method = 'GET',
    headers = headers,
    sink = ltn12.sink.table(res)
  }
  if status_code == 200 then
    res = table.concat(res)
    data_table = json:decode(res)
    grab_id = data_table[constants['GRAB_ID_RESPONSE_KEY']]
    ngx.req.set_header(constants['GRAB_ID_KEY'], grab_id)
  else
    responses.send(status_code, res)
  end
end

return _M
