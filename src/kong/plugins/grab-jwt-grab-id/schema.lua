return {
  no_consumer = false, -- this plugin is available on APIs as well as on Consumers,
  fields = {
    api_url = { required = true, type = "string"}
  },
  self_check = function(schema, plugin_t, dao, is_updating)
    return true
  end
}
