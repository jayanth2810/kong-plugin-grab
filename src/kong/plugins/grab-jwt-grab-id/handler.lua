-- Getting the kong base plugin --
local BasePlugin = require "kong.plugins.base_plugin"
-- Creating a new plugin handler by extending base plugin
local GrabJWTHandler = BasePlugin:extend()

local access = require "kong.plugins.grab-jwt-grab-id.access"

local plugin_name = "grab-jwt-grab-id"

function GrabJWTHandler:new()
  GrabJWTHandler.super.new(self, plugin_name)
end

-- Executed for every request
function GrabJWTHandler:access(plugin_conf)
  GrabJWTHandler.super.access(self)
  access.execute(plugin_conf)
end

GrabJWTHandler.PRIORITY = 1000

return GrabJWTHandler
