local JSON = require "JSON"

local get_body = ngx.req.get_body_data

local _M = {}

function _M.execute(conf)
    _M.add_service_id_to_request_body(conf)
end

function _M.add_service_id_to_request_body(conf)
    ngx.req.read_body()
    local body_data = tostring(get_body())
    
    print("Original Request Body " .. body_data)
    
    local lua_table_request_body = JSON:decode(body_data)
    lua_table_request_body["serviceID"] = tostring(conf.service_id)
    
    local raw_json_body_data    = JSON:encode(lua_table_request_body)
    ngx.req.set_body_data(raw_json_body_data)
    
    print("Modified Request Body " .. raw_json_body_data)
end

return _M