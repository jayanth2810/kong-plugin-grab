-- If you're not sure your plugin is executing, uncomment the line below and restart Kong
-- then it will throw an error which indicates the plugin is being loaded at least.

--assert(ngx.get_phase() == "timer", "The world is coming to an end!")

-- load the base plugin object and create a subclass
local BasePlugin = require "kong.plugins.base_plugin"
local GrabLoginJWTHandler = BasePlugin:extend()

local access = require "kong.plugins.grab-login-jwt.access"

--local access = require "kong.plugins.grab-login-jwt.access"
local plugin_name = "grab-login-jwt"

function GrabLoginJWTHandler:new()
  GrabLoginJWTHandler.super.new(self, plugin_name)
end


function GrabLoginJWTHandler:access(plugin_conf) --- Executed for every request from a client and before it is being proxied to the upstream service
  GrabLoginJWTHandler.super.access(self)
  
  -- custom logic starts
  access.execute(plugin_conf)
  -- custom logic ends
  
end

function GrabLoginJWTHandler:header_filter(plugin_conf) --- Executed when all response headers bytes have been received from the upstream service
  GrabLoginJWTHandler.super.access(self)
end

GrabLoginJWTHandler.PRIORITY = 1000 -- set the plugin priority, which determines plugin execution order

return GrabLoginJWTHandler -- return our plugin object
