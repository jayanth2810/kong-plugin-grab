package = "grab-plugin"
version = "0.1.0-1"
-- The version '0.1.0' is the source code version, the trailing '1' is the version of this rockspec.
-- whenever the source version changes, the rockspec should be reset to 1. The rockspec version is only
-- updated (incremented) when this file changes, but the source remains the same.

supported_platforms = {"linux", "macosx"}
source = {
  url = "https://bitbucket.org/jayanth2810/kong-plugin-grab.git",
  tag = "0.1.0"
}

description = {
  summary = "Plugins for Grab Pay API gate way",
  homepage = "http://getkong.org",
  license = "MIT"
}

dependencies = {
    "json-lua==0.1-3"
}

build = {
  type = "builtin",
  modules = {
    ["kong.plugins.grab-login-jwt.handler"] = "kong/plugins/grab-login-jwt/handler.lua",
    ["kong.plugins.grab-login-jwt.schema"] = "kong/plugins/grab-login-jwt/schema.lua",
    ["kong.plugins.grab-login-jwt.access"] = "kong/plugins/grab-login-jwt/access.lua",

    ["kong.plugins.grab-jwt-grab-id.handler"] = "kong/plugins/grab-jwt-grab-id/handler.lua",
    ["kong.plugins.grab-jwt-grab-id.schema"] = "kong/plugins/grab-jwt-grab-id/schema.lua",
    ["kong.plugins.grab-jwt-grab-id.access"] = "kong/plugins/grab-jwt-grab-id/access.lua"
  }
}
