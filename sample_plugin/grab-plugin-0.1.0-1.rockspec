package = "grab-plugin"
version = "0.1.0-1"
-- The version '0.1.0' is the source code version, the trailing '1' is the version of this rockspec.
-- whenever the source version changes, the rockspec should be reset to 1. The rockspec version is only
-- updated (incremented) when this file changes, but the source remains the same.

supported_platforms = {"linux", "macosx"}
source = {
  url = "https://bitbucket.org/jayanth2810/kong-plugin-grab.git",
  tag = "0.1.0"
}

description = {
  summary = "Kong is a scalable and customizable API Management Layer built on top of Nginx.",
  homepage = "http://getkong.org",
  license = "MIT"
}

dependencies = {
}

build = {
  type = "builtin",
  modules = {
    ["kong.plugins.myplugin.handler"] = "kong/plugins/myplugin/handler.lua", -- The sample plugin name is called "myplugin"
    ["kong.plugins.myplugin.schema"] = "kong/plugins/myplugin/schema.lua",
  }
}
